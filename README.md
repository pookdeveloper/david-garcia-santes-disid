## ---------- Ejecucion mediante docker ----------

Ejecutar el comnado:
* docker-compose up --build

Acceder a la url:
* [localhost:80](localhost:80)
  


## ---------- Alternativa a docker ----------
Ejecutar dentro de la carpeta disid-empleados-loopback3: (Puede que necesiteis instalar npm install -g loopback-cli)
* cd disid-empleados-loopback3
* npm install
* npm start

Ejecutar dentro de la carpeta disid-empleados-angular:
* cd disid-empleados-angular
* ng install
* ng serve

Acceder a la url:
* [localhost:3000](localhost:4200)
