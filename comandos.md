# Comandos docker
docker-compose up --build

docker-compose down -v

docker system prune -a

docker container ls

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker rmi $(docker images -a -q)

