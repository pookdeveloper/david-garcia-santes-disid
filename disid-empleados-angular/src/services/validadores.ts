import { AbstractControl } from '@angular/forms';

export function ValidarEdad(control: AbstractControl) {
  if (+control.value >= 18) {
    null
  } else {
    return {
      ValidarEdad: {
        valid: false
      }
    }
  }
}