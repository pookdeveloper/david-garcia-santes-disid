
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { global } from './global';
import { Empleado } from '../entities/empleado.entity';
import { Observable } from 'rxjs';
import { EmpleadosDepartamentos } from '../entities/empleadosDepartamentos';


@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private http: HttpClient) {
  }

  // Datos fijos de las llamadas 
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    params: null
  };


  /**
   * @param  {Empleado} empleado
   */
  add(empleado: Empleado): Observable<Empleado> {
    delete empleado.id;
    console.log(global.url + "empleados");
    return this.http.post<Empleado>(global.url.concat('empleados'), empleado, this.httpOptions)
  }


  /**
   * @param  {Empleado} empleado
   */
  addDepartamento(empleadoDepartamento: EmpleadosDepartamentos) {
    delete empleadoDepartamento.id;
    console.log(global.url + "empleados");
    return this.http.post(global.url.concat('empleados_departamentos'), empleadoDepartamento, this.httpOptions)
  }

  /**
   * @param  {Empleado} empleado
   */
  update(empleado: Empleado): Observable<Empleado> {
    return this.http.put<Empleado>(global.url.concat('empleados/').concat(`${empleado.id}/`), empleado, this.httpOptions)
  }


  /**
   * @param  {EmpleadosDepartamentos} empleadoDepartamento
   */
  updateDepartamento(empleadoDepartamento: EmpleadosDepartamentos) {
    return this.http.put(global.url.concat('empleados_departamentos/').concat(`${empleadoDepartamento.id}/`), empleadoDepartamento, this.httpOptions)
  }


  /**
   * @param  {number} id
   */
  get(id: number): Observable<Empleado> {
    let include: any = { relation: "empleadosDepartamentos" };

    let filter = Object();
    filter.include = include

    var httpOptions = Object.assign({}, this.httpOptions);
    let parametros = Object();
    parametros.filter = JSON.stringify(filter);
    httpOptions.params = parametros;

    return this.http.get<Empleado>(global.url.concat('empleados/').concat(`${id}/`), httpOptions);
  }

  /**
   * @param  {number} id
   */
  delete(id: number) {
    return this.http.delete(global.url.concat('empleados/').concat(`${id}/`), this.httpOptions)
  }


  /**
   * @param  {number} id
   */
  deleteDepartamento(id: number) {
    console.log(global.url + "empleados");
    return this.http.delete(global.url.concat('empleados_departamentos/').concat(`${id}/`), this.httpOptions)
  }

}

