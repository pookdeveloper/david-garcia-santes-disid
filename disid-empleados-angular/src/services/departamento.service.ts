import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global } from './global';
import { Departamento } from 'src/entities/departamento.entity';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  constructor(private http: HttpClient) { }

  // Datos fijos de las llamadas 
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    params: null
  };


  /**
   * @param  {Departamento} departamento
   */
  add(departamento: Departamento) {
    delete departamento.id;
    console.log(global.url + "departamentos");
    return this.http.post(global.url + "departamentos", departamento, this.httpOptions)
  }

  /**
   * @param  {Departamento} departamento
   */
  update(departamento: Departamento) {
    return this.http.put(global.url + "departamentos/" + departamento.id, departamento, this.httpOptions)
  }


  /**
   * @param  {number} id
   */
  get(id: number) {
    return this.http.get(global.url + "departamentos/" + id, this.httpOptions)
  }

  /**
   * @param  {number} id
   */
  delete(id: number) {
    return this.http.delete(global.url + "departamentos/" + id, this.httpOptions)
  }

}

