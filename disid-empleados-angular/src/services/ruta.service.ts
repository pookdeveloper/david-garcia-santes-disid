import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class RutaService {

  constructor(
    public router: Router,
    private __location: Location
  ) { }


  obtenerRutaActual() {
    return this.__location.path().substring(0, this.__location.path(false).lastIndexOf("/") + 1);
  }

  goBack() {
    console.log("Volvemos a la ruta anterior");
    this.__location.back();
  }

  irRutaModulo(ruta: string) {
    this.__location.go(ruta);
  }

  irRuta(ruta: string) {
    this.router.navigate([ruta])
  }

}
