
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Operador, Parametro } from '../entities/parametro.model';
import { global } from './global';


@Injectable({
    providedIn: 'root'
})

// Clase para funciones genéricas de utilería

export class UtilsService {

    // Datos fijos de las llamadas
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        params: null
    };

    constructor(private http: HttpClient) {
    }


    /**
     * @param  {HttpErrorResponse} e
     * @returns Object
     */
    errorToInfo(e: HttpErrorResponse): Object {
        let info = {};
        // Código de error
        info['status'] = e.status;
        // Texto del status
        info['statusText'] = e.statusText;
        // Mensaje del objeto error
        info['errorMessage'] = e.message;
        // Mensaje devuelto por loopback
        info['loopbackMessage'] = e.error.error.message;
        return info;
    }


    /** Devuelve una plantilla de error para mostrarla cuando se produce un error durante las llamadas a la API
     * @param  {HttpErrorResponse} e
     * @returns string
     */
    getTemplateNotificationError(e: HttpErrorResponse): string {
        let myError = this.errorToInfo(e);
        let template = "<b>Código: </b>" + myError['status'] + "<br>" +
            "<b>Error: </b>" + myError['statusText'] + "<br>" +
            "<b>Mensaje API: </b>" + myError['loopbackMessage'] + "<br><hr/>" + myError['errorMessage'];
        return template;
    }


    /** Compone un operador para las clausulas where
     * @param  {Parametro} elemento
     * @returns Object
     */
    componeOperador(elemento: Parametro): Object {
        let objeto = new Object()
        switch (elemento.operador) {
            case Operador.regexp: {
                let v = elemento.valor.replace(/\*/g, "(.)*").replace(/\?/g, "(.){1}");
                objeto[elemento.campo] = { regexp: v }
                break;
            }
            case Operador.equal: {
                objeto[elemento.campo] = elemento.valor
                break;
            }
            case Operador.gte: {
                objeto[elemento.campo] = { gte: elemento.valor }
                break;
            }
            case Operador.gt: {
                objeto[elemento.campo] = { gt: elemento.valor }
                break;
            }
            case Operador.lt: {
                objeto[elemento.campo] = { lt: elemento.valor }
                break;
            }
            case Operador.lte: {
                objeto[elemento.campo] = { lte: elemento.valor }
                break;
            }
        }
        return objeto
    }


    /**
     * @param  {Parametro[]} filtro
     * @returns Object
     */
    composicionFiltroMain(filtro: Parametro[]): Object | null {
        let filtroRegistros = {};
        // Composición del filtro de registro
        if (filtro != null && filtro.length > 0) {
            filtro.forEach((elemento) => {
                console.log(elemento)
                let objeto = this.componeOperador(elemento);
                Object.assign(filtroRegistros, objeto);
            })
        }
        return (filtroRegistros) ? filtroRegistros : null;
    }


    /**
     * @param  {string} tabla
     * @param  {Parametro[]} filtroMain?
     * @param  {any} include?
     */
    getRecordsFilter(tabla: string, filtroMain?: Parametro[], include?: any): Observable<any[]> {

        let filter = Object();
        filter.where = this.composicionFiltroMain(filtroMain);
        (filter.where) ? filter.where : delete filter.where;
        filter.include = include

        var httpOptions = Object.assign({}, this.httpOptions);
        let parametros = Object();
        parametros.filter = JSON.stringify(filter);
        httpOptions.params = parametros;

        return this.http.get<any[]>(global.url.concat(tabla), httpOptions);
    }


    /**
     * @param  {string} tabla
     */
    getRecords(tabla: string) {
        console.log("URL AL FINAL", global.url + tabla)
        return this.http.get(global.url.concat(tabla), this.httpOptions);
    }

    /** Control de inputs numericos
     * @param  {} event
     * @returns boolean
     */
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }


}
