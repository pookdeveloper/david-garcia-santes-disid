import { EmpleadosDepartamentos } from './empleadosDepartamentos';

export class Empleado {

  constructor(
    public id: number,
    public name: string,
    public lastname: string,
    public age: number,
    public fecha_alta: string,
    public empleadosDepartamentos?: EmpleadosDepartamentos
  ) {

  }



}