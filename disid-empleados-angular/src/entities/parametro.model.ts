export enum Operador {
    regexp,
    equal,
    gt,
    gte,
    lt,
    lte
}

export class Parametro {
    campo: string;
    valor: any;
    operador: Operador;

    constructor(campo: string, valor: any, operador = Operador.regexp) {
        this.campo = campo
        this.valor = valor
        this.operador = operador
    }
}

export class Include {
    relation: string;
    tipoJoin: string;
    where: Parametro[];
    include: any;

    constructor(relation: string, tipoJoin = 'inner') {
        this.relation = relation;
        this.tipoJoin = tipoJoin;
        this.where = [];
        this.include = {};
    }
}
