import { Empleado } from './empleado.entity';

export class Departamento {

  constructor(
    public id: number,
    public name: string,
    public empleados?: Array<Empleado>[] | null
  ) {


  }



}