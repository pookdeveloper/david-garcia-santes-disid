import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  loginloading: boolean = false;
  return: string = '';
  error_login = false

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

}
