import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacionEmpleadosComponent } from './modificacion-empleados.component';

describe('ModificacionEmpleadosComponent', () => {
  let component: ModificacionEmpleadosComponent;
  let fixture: ComponentFixture<ModificacionEmpleadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificacionEmpleadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacionEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
