import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreacionDepartamentosComponent } from './creacion-departamentos/creacion-departamentos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepartamentosRoutingModule } from './departamentos-routing.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    CreacionDepartamentosComponent
  ],
  imports: [
    NgZorroAntdModule, // IMPORTANTE! Deberiamos solo añadir los modulos necesario no todo..
    DepartamentosRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DepartamentosModule { }
