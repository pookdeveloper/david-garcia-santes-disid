
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreacionDepartamentosComponent } from './creacion-departamentos/creacion-departamentos.component';


const routes: Routes = [
  {
    path: "", redirectTo: "creacion",
  },
  {
    path: "creacion",
    component: CreacionDepartamentosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartamentosRoutingModule { }
