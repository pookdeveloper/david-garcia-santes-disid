import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd';
import { Departamento } from 'src/entities/departamento.entity';
import { UtilsService } from 'src/services/utils.service';
import { DepartamentoService } from '../../../services/departamento.service';
import { RutaService } from '../../../services/ruta.service';


@Component({
  selector: 'app-creacion-departamentos',
  templateUrl: './creacion-departamentos.component.html',
  styleUrls: ['./creacion-departamentos.component.scss']
})
export class CreacionDepartamentosComponent implements OnInit {

  formulario: FormGroup;
  departamentos: Departamento[];

  constructor(
    private __notificationService: NzNotificationService,
    private __utilsService: UtilsService,
    private __departamentoService: DepartamentoService
  ) {
  }


  ngOnInit() {
    this.createForm()
    this.cargarDepartamentos();
  }


  /** Creacion de formularios
   */
  createForm() {
    this.formulario = new FormGroup({
      name: new FormControl('', [Validators.required])
    });
  }


  /** Guardado del formulario
   */
  save() {
    if (this.formulario.valid) {

      let name = this.formulario.value.name;
      const departamento: Departamento = new Departamento(null, name);

      this.__departamentoService.add(departamento).subscribe(
        response => {
          console.log(response)
          this.formulario.reset();
          this.__notificationService.success("Registro creado", "El registro se ha creado con éxito.", { nzDuration: 3500 });
        },
        error => {
          console.log(error);
          this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
        })
    }
    else {
      // Actualizamos la presentación de errores en pantalla 
      for (let k in this.formulario.controls) {
        this.formulario.controls[k].markAsDirty();
        this.formulario.controls[k].updateValueAndValidity();
      }
    }
  }


  /** Cargar departamentos
   */
  cargarDepartamentos() {
    this.__utilsService.getRecords('departamentos').subscribe(
      response => {
        console.log(response)
        this.departamentos = response as Departamento[];
      });
  }

}
