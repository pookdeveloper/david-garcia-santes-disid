import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreacionDepartamentosComponent } from './creacion-departamentos.component';

describe('CreacionDepartamentosComponent', () => {
  let component: CreacionDepartamentosComponent;
  let fixture: ComponentFixture<CreacionDepartamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreacionDepartamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreacionDepartamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
