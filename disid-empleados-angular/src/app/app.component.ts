import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'disid-empleados-angular';
  isCollapsed = false;
  isReverseArrow = false;
  triggerTemplate = null;
  opened = true;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

}
