import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [

  {
    path: "inicio", loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioModule),
  },
  {
    path: "login", component: LoginComponent
  },
  {
    path: "", redirectTo: "inicio/empleados/listado", pathMatch: 'full'
  },

  {
    path: "**", redirectTo: "inicio"
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
