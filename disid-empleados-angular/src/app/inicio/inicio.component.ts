import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  isCollapsed = false;
  isReverseArrow = false;
  triggerTemplate = null;
  subscription: Subscription;

  opened = true;
  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }


}
