import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio.component';

const routes: Routes = [
  {
    path: "", component: InicioComponent,
    children: [
      {
        path: "empleados",
        loadChildren: () => import('../empleados/empleados.module').then(m => m.EmpleadosModule)
      },
      {
        path: "departamentos",
        loadChildren: () => import('../departamentos/departamentos.module').then(m => m.DepartamentosModule)
      }
    ]

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InicioRoutingModule { }
