import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SidenavComponent } from '../sidenav/sidenav.component';
import { InicioRoutingModule } from './inicio-routing.module';
import { InicioComponent } from './inicio.component';


@NgModule({
  declarations: [
    InicioComponent,
    SidenavComponent,
  ],
  imports: [
    InicioRoutingModule,
    NgZorroAntdModule, NgZorroAntdModule, // IMPORTANTE! Deberiamos solo añadir los modulos necesario no todo..
    CommonModule
  ],
  exports: []

})
export class InicioModule { }
