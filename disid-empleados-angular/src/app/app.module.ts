import { InicioModule } from './inicio/inicio.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
/* import { SidenavComponent } from './sidenav/sidenav.component'; */
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
    /* SidenavComponent, */
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule, NgZorroAntdModule, // IMPORTANTE! Deberiamos solo añadir los modulos necesario no todo..
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: es_ES }],
  bootstrap: [AppComponent]
})
export class AppModule { }
