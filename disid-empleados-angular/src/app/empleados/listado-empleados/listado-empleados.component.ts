import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { DepartamentoService } from 'src/services/departamento.service';
import { EmpleadoService } from 'src/services/empleado.service';
import { UtilsService } from 'src/services/utils.service';
import { Operador, Parametro } from '../../../entities/parametro.model';
import { Empleado } from './../../../entities/empleado.entity';
import { Departamento } from 'src/entities/departamento.entity';



@Component({
  selector: 'app-listado-empleados',
  templateUrl: './listado-empleados.component.html',
  styleUrls: ['./listado-empleados.component.scss']
})
export class ListadoEmpleadosComponent implements OnInit {

  titulo: string;                    // Nombre descriptivo de la vista que aparecerá en el encabezado
  dataSet: any;                    // Relación de registros de la tabla  
  filtroForm: FormGroup;             // Formulario para filtrar
  cargando: boolean;                   // Indica si se están cargando datos o no
  departamentos: Departamento[];
  selectedValue: any = "-1";

  constructor(fb: FormBuilder,
    private __utilsService: UtilsService,
    private __notificationService: NzNotificationService,
    private __modalService: NzModalService,
    private __departamentoService: DepartamentoService,
    private __empleadoService: EmpleadoService
  ) {

    this.titulo = "Listado de empleados";
    this.cargando = true;

    this.filtroForm = fb.group({
      'name': [''],
      'lastname': [''],
      'age': [''],
      'fecha_alta': [null],
      'id_departamento': [null]
    });
  }


  ngOnInit() {
    this.cargarRegistros();
  }

  // Función de inicialización utilizada para realizar la carga inicial de personas de la BBDD
  cargarRegistros() {
    this.cargarDepartamentos();
    this.cargaPagina(null, { relation: "empleadosDepartamentos" });
  }

  cargarDepartamentos() {
    this.cargando = true;
    this.__utilsService.getRecords('departamentos').subscribe(
      response => {
        console.log(response)
        this.departamentos = response as Departamento[];
        this.departamentos.unshift({ id: -1, name: "Todos" })
        this.selectedValue = "-1";
      });
  }

  /**
   * @param  {} filtroMain
   * @param  {} include
   */
  cargaPagina(filtroMain, include) {
    this.cargando = true;
    this.__utilsService.getRecordsFilter('empleados', filtroMain, include).subscribe(
      response => {

        let id_departamento = (this.filtroForm.value.id_departamento == "-1") ? null : this.filtroForm.value.id_departamento;

        if (include) {
          this.dataSet = response as any[];
          if (id_departamento) {
            this.dataSet = this.dataSet.filter(element => {
              return (element.empleadosDepartamentos && element.empleadosDepartamentos.id_departamento == this.filtroForm.value.id_departamento)
            });
          }
        } else {
          this.dataSet = response as any[];
        }

        this.cargando = false;
      },
      error => {
        this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
        this.cargando = false;
      }
    );
  }


  /** Reseta los filtros
   */
  resetFiltro() {
    this.filtroForm.reset();
    this.filtroForm.controls['id_departamento'].setValue("-1")
    this.cargaPagina(null, { relation: "empleadosDepartamentos" })
  }


  /** Aplicamos el filtro de busqueda
   */
  aplicarFiltro() {

    let filtroMain: Parametro[] = [];
    let parametro: Parametro;

    let name = (this.filtroForm.controls['name'].value != null) ? this.filtroForm.controls['name'].value.trim() : null;
    let lastname = (this.filtroForm.controls['lastname'].value != null) ? this.filtroForm.controls['lastname'].value.trim() : null;
    let age = (this.filtroForm.controls['age'].value != null) ? this.filtroForm.controls['age'].value.trim() : null;
    let fecha_alta = (this.filtroForm.controls['fecha_alta'].value != null) ? this.filtroForm.controls['fecha_alta'].value : null;
    let id_departamento = (this.filtroForm.controls['id_departamento'].value != null) ? this.filtroForm.controls['id_departamento'].value : null;

    let fecha_alta_ISO = null
    if (fecha_alta) {
      fecha_alta.setHours(0)
      fecha_alta.setMinutes(0)
      fecha_alta.setSeconds(0)
      fecha_alta_ISO = fecha_alta.toISOString()
    }

    if (name != "" && name != null) {
      parametro = new Parametro('name', name, Operador.regexp)
      filtroMain.push(parametro)
    }
    if (lastname != "" && lastname != null) {
      parametro = new Parametro('lastname', lastname, Operador.regexp)
      filtroMain.push(parametro)
    }
    if (age != "" && age != null) {
      parametro = new Parametro('age', age, Operador.equal)
      filtroMain.push(parametro)
    }
    if (fecha_alta_ISO != null) {
      console.log(fecha_alta_ISO)
      parametro = new Parametro('fecha_alta', fecha_alta_ISO, Operador.gte)
      filtroMain.push(parametro)
    }
    if (id_departamento != null) {
      console.log(id_departamento)
      var include = { relation: "empleadosDepartamentos" }
    }
    this.cargaPagina(filtroMain, include);
  }

  /**
   * @param  {Empleado} empleado
   */
  eliminar(empleado: Empleado) {

    this.__modalService.warning({
      nzTitle: "Eliminar registro",
      nzContent: "Está a punto de eliminar el registro.<br/><br/><b>¿Desea continuar?",
      nzOkText: "Eliminar",
      nzCancelText: "Cancelar",
      nzOnOk: () => {

        this.__empleadoService.deleteDepartamento(empleado.empleadosDepartamentos.id).subscribe(
          response => {
            this.__empleadoService.delete(empleado.id).subscribe(
              response => {
                this.__notificationService.success("Registro borrado", "El registro se ha borrado con éxito.", { nzDuration: 3500 });
                this.cargarRegistros();
              },
              error => {
                console.log(error);
                this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
              });
          },
          error => {
            console.log(error);
            this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
          });
      }
    });
  }


}


