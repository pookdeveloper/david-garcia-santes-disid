import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacionEmpleadoComponent } from './modificacion-empleado.component';

describe('ModificacionEmpleadoComponent', () => {
  let component: ModificacionEmpleadoComponent;
  let fixture: ComponentFixture<ModificacionEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificacionEmpleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacionEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
