import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { Departamento } from 'src/entities/departamento.entity';
import { Empleado } from 'src/entities/empleado.entity';
import { EmpleadosDepartamentos } from 'src/entities/empleadosDepartamentos';
import { EmpleadoService } from 'src/services/empleado.service';
import { UtilsService } from 'src/services/utils.service';
import { RutaService } from '../../../services/ruta.service';
import { ValidarEdad } from '../../../services/validadores';

@Component({
  selector: 'app-modificacion-empleado',
  templateUrl: '../creacion-empleado/creacion-empleado.component.html',
  styleUrls: ['../creacion-empleado/creacion-empleado.component.scss']
})
export class ModificacionEmpleadoComponent implements OnInit {

  id: number
  formulario: FormGroup
  departamentos: Departamento[];
  selectedValue: any
  empleado: Empleado;

  constructor(
    private route: ActivatedRoute,
    private __rutaService: RutaService,
    private __utilsService: UtilsService,
    private __notificationService: NzNotificationService,
    private __empleadoService: EmpleadoService

  ) {
  }


  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.obtenerElemento(this.id)
  }


  /** Obtememos el elemento a modificar
   * @param  {} id
   */
  obtenerElemento(id) {
    this.__utilsService.getRecords('departamentos').subscribe(
      response => {
        console.log(response)
        this.departamentos = response as Departamento[];

        console.log("obtenerElemento", id);
        this.__empleadoService.get(id).subscribe(
          response => {
            console.log("response", response);

            this.empleado = response;
            this.selectedValue = String(response.empleadosDepartamentos.id_departamento);

            this.formulario = new FormGroup({
              name: new FormControl(this.empleado.name, [Validators.required]),
              lastname: new FormControl(this.empleado.lastname, [Validators.required]),
              age: new FormControl(this.empleado.age, [Validators.required, ValidarEdad]), // Validators.min(18)
              fecha_alta: new FormControl(this.empleado.fecha_alta, [Validators.required]),
              id_departamento: new FormControl(this.selectedValue, [Validators.required])
            })
          },
          error => {
            this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
            console.log("error", error);
          }
        );
      });

  }

  /** Guardamos los datos
   */
  save() {
    if (this.formulario.valid) {
      let name = this.formulario.value.name;
      let lastname = this.formulario.value.lastname;
      let age = Number(this.formulario.value.age);
      let fecha_alta = new Date(this.formulario.value.fecha_alta);
      fecha_alta.setHours(0);
      fecha_alta.setMinutes(0);
      fecha_alta.setSeconds(0);
      let fecha_alta_ISO = fecha_alta.toISOString();
      let id_departamento = this.formulario.value.id_departamento;

      this.empleado = Object.assign(this.empleado, this.formulario.value);

      this.__empleadoService.update(this.empleado).subscribe(
        response => {

          let empleadoDeptartameno: EmpleadosDepartamentos = new EmpleadosDepartamentos(this.empleado.empleadosDepartamentos.id, response.id, id_departamento);

          this.__empleadoService.updateDepartamento(empleadoDeptartameno).subscribe(
            response => {
              this.__notificationService.success("Registro creado", "El registro se ha creado con éxito.", { nzDuration: 3500 });
            },
            error => {
              console.log(error);
              this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
            }
          )
        },
        error => {
          console.log(error);
          this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
        }
      )

    }
    else {
      // Actualizamos la presentación de errores en pantalla 
      for (let k in this.formulario.controls) {
        this.formulario.controls[k].markAsDirty();
        this.formulario.controls[k].updateValueAndValidity();
      }
    }
  }


}



