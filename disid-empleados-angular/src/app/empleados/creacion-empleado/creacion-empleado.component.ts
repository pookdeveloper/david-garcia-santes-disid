import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { UtilsService } from 'src/services/utils.service';
import { Empleado } from '../../../entities/empleado.entity';
import { EmpleadoService } from '../../../services/empleado.service';
import { EmpleadosDepartamentos } from 'src/entities/empleadosDepartamentos';
import { RutaService } from '../../../services/ruta.service';
import { ValidarEdad } from '../../../services/validadores';
import { Departamento } from 'src/entities/departamento.entity';


@Component({
  selector: 'app-creacion-empleado',
  templateUrl: './creacion-empleado.component.html',
  styleUrls: ['./creacion-empleado.component.scss']
})
export class CreacionEmpleadoComponent implements OnInit {


  formulario: FormGroup;
  departamentos: Departamento[]

  constructor(
    private __notificationService: NzNotificationService,
    private __utilsService: UtilsService,
    private __rutaService: RutaService,
    private __empleadoService: EmpleadoService
  ) {
  }


  ngOnInit() {
    this.createForm()
    this.cargarDepartamentos()
  }


  /** creamos el formulario
   */
  createForm() {
    this.formulario = new FormGroup({
      name: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required, ValidarEdad]), // Validators.min(18)
      fecha_alta: new FormControl(null, [Validators.required]),
      id_departamento: new FormControl(null, [Validators.required])
    });
  }


  /** Guardamos los datos
   */
  save() {
    if (this.formulario.valid) {
      let name = this.formulario.value.name;
      let lastname = this.formulario.value.lastname;
      let age = Number(this.formulario.value.age);
      let fecha_alta = new Date(this.formulario.value.fecha_alta);
      fecha_alta.setHours(0);
      fecha_alta.setMinutes(0);
      fecha_alta.setSeconds(0);
      let fecha_alta_ISO = fecha_alta.toISOString();
      let id_departamento = this.formulario.value.id_departamento;

      const empleado: Empleado = new Empleado(null, name, lastname, age, fecha_alta_ISO);

      this.__empleadoService.add(empleado).subscribe(
        response => {

          let empleadoDeptartameno: EmpleadosDepartamentos = new EmpleadosDepartamentos(null, response.id, id_departamento);

          this.__empleadoService.addDepartamento(empleadoDeptartameno).subscribe(
            response => {
              this.formulario.reset();
              this.__notificationService.success("Registro creado", "El registro se ha creado con éxito.", { nzDuration: 3500 });
            },
            error => {
              console.log(error);
              this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
            })
        },
        error => {
          console.log(error);
          this.__notificationService.create("error", "Se ha producido un error", this.__utilsService.getTemplateNotificationError(error), { nzDuration: 0 });
        })
    } else {
      // Actualizamos la presentación de errores en pantalla 
      for (let k in this.formulario.controls) {
        this.formulario.controls[k].markAsDirty();
        this.formulario.controls[k].updateValueAndValidity();
      }
    }
  }



  /** Cargamos los departamentos
   */
  cargarDepartamentos() {
    this.__utilsService.getRecords('departamentos').subscribe(
      response => {
        console.log(response)
        this.departamentos = response as Departamento[];
      });
  }

}
