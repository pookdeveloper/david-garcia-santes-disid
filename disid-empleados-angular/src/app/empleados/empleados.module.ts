import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CreacionEmpleadoComponent } from './creacion-empleado/creacion-empleado.component';
import { EmpleadosRoutingModule } from './empleados-routing.module';
import { ListadoEmpleadosComponent } from './listado-empleados/listado-empleados.component';
import { ModificacionEmpleadoComponent } from './modificacion-empleado/modificacion-empleado.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModificacionEmpleadosComponent } from '../modificacion-empleados/modificacion-empleados.component';

@NgModule({
  declarations: [
    CreacionEmpleadoComponent,
    ModificacionEmpleadoComponent,
    ListadoEmpleadosComponent,
    ModificacionEmpleadosComponent
  ],
  imports: [
    NgZorroAntdModule, NgZorroAntdModule, // IMPORTANTE! Deberiamos solo añadir los modulos necesario no todo..
    EmpleadosRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EmpleadosModule { }
