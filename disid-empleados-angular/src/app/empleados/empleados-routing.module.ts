
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosModule } from './empleados.module';
/* import { ListadoEmpleadosComponent } from './listado-empleados/listado-empleados.component';
import { ModificacionEmpleadoComponent } from './modificacion-empleado/modificacion-empleado.component';
 */import { CreacionEmpleadoComponent } from './creacion-empleado/creacion-empleado.component';
import { ListadoEmpleadosComponent } from './listado-empleados/listado-empleados.component';
import { ModificacionEmpleadoComponent } from './modificacion-empleado/modificacion-empleado.component';


const routes: Routes = [
  {
    path: "", redirectTo: "listado",
  },
  {
    path: "listado",
    component: ListadoEmpleadosComponent
  },
  {
    path: "creacion",
    component: CreacionEmpleadoComponent
  },
  {
    path: "modificacion/:id",
    component: ModificacionEmpleadoComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
